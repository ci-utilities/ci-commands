# GitLab API for GitLab CI

GitLab API utilities useful in GitLab CI scripts, packaged as a docker image

## Artifacts download

`docker run registry.gitlab.com/cirecipes/gitlab-api --token TOKEN artifacts download ARTIFACTS DESTINATION`

Downloads `ARTIFACTS` and unarchives the artifacts to `DESTINATION`.

`ARTIFACTS` can be one of the following:

* `NAMESPACE/PROJECT/-/JOB_ID` — downloads artifacts for specific job (by ID) in a project. For example: user/project/-/123
* `NAMESPACE/PROJECT/PIPELINE_ID/JOB_NAME` — downloads artifacts for the (latest successful) job of a given name within a specific pipeline (by ID). For example: user/project/123/build
* `NAMESPACE/PROJECT/REF@latest/JOB_NAME` — downloads artifacts for the (latest successful) job of a given name within the latest pipeline started for a given ref. For example: user/project/branch@latest/build

## Pipeline trigger

`docker run registry.gitlab.com/cirecipes/gitlab-api --token TOKEN pipeline trigger PIPELINE [VARIABLE …]`

Triggers `PIPELINE` and passes it one or more variables. `PIPELINE` is `NAMESPACE/PROJECT/PIPELINE_NAME`. Variables are of the form `key=value`.


