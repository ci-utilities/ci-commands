#!/usr/bin/env python3

from argparse import ArgumentParser
from urllib.request import urlretrieve, Request, build_opener, install_opener, urlopen
from urllib.parse import quote, urlencode
import re
from os import environ
import logging as log
from zipfile import ZipFile
import json

log.basicConfig(level=log.DEBUG, format="%(message)s")


def retrieveAPI(gitlab, token, endpoint, **query):
    headers = dict()
    if token is not None:
        headers["PRIVATE-TOKEN"] = token

    url = f"{ gitlab}{ endpoint }?{ urlencode(query) }"
    log.info(f"Retrieving URL: { url }")

    # Janky… see https://stackoverflow.com/questions/45247983/urllib-urlretrieve-with-custom-header
    opener = build_opener()
    opener.addheaders = list(headers.items())
    install_opener(opener)
    return urlretrieve(url)


def postAPI(gitlab, token, endpoint, **query):
    headers = dict()
    if token is not None:
        query.update(token=token)

    url = f"{ gitlab}{ endpoint }"
    data = urlencode(query)
    log.info(f"Posting to URL: { url } [{ data }]")
    return urlopen(Request(url, data=data.encode("ascii"), headers=headers)).read()


def jsonAPI(gitlab, token, endpoint, **query):
    headers = dict()
    if token is not None:
        headers["PRIVATE-TOKEN"] = token

    url = f"{ gitlab}{ endpoint }?{ urlencode(query) }"
    log.info(f"Retrieving JSON: { url }")

    # Janky… see https://stackoverflow.com/questions/45247983/urllib-urlretrieve-with-custom-header
    opener = build_opener()
    opener.addheaders = list(headers.items())
    install_opener(opener)
    response, headers = urlretrieve(url)
    response = json.load(open(response))
    return response


def quoteAll(s):
    return quote(s, safe=[])


def artifactsParser(subparsers):
    parser = subparsers.add_parser("artifacts")
    subparsers = parser.add_subparsers()

    def downloadParser(subparsers):
        parser = subparsers.add_parser("download")
        parser.add_argument("jobRef")
        parser.add_argument("destination")

        def download(jobRef, destination, gitlab, token):
            # Project is `namespace/name` or id
            # Pipeline is pipeline id, or ref@latest, or '-'
            # Job is name or id
            # If pipeline is -, job must be job id
            project, pipeline, job = jobRef.rsplit("/", 2)

            if pipeline == "-":
                pipeline = ""

            m = re.match(r"(.*)@latest", pipeline)
            # If pipeline is given, and it's an @latest reference, then the pipeline ID + job name have to be resolved to job ID
            if pipeline and not m:
                # Pick the latest (by creation date) job from successful ones matching the job name
                jobs = jsonAPI(
                    gitlab,
                    token,
                    f"/api/v4/projects/{ quoteAll(project) }/pipelines/{ quoteAll(pipeline) }/jobs",
                    scope="success",
                )
                jobs = [j for j in jobs if j["name"] == job]
                jobs = sorted(jobs, key=lambda j: j["created_at"], reverse=True)
                job = str(jobs[0]["id"])
                pipeline = None

            # If pipeline is unresolved at this point, it's a "latest" reference, and we'll get the artifacts through artifacts API
            if pipeline:
                (artifacts, _) = retrieveAPI(
                    gitlab,
                    token,
                    f"/api/v4/projects/{ quoteAll(project) }/jobs/artifacts/{ quoteAll(m.group(1)) }/download",
                    job=job,
                )
            # Otherwise job must be a proper job ID, and we'll get the artifacts through jobs API
            else:
                (artifacts, _) = retrieveAPI(
                    gitlab,
                    token,
                    f"/api/v4/projects/{ quoteAll(project) }/jobs/{ quoteAll(job) }/artifacts",
                )

            with ZipFile(artifacts, "r") as zipFile:
                zipFile.extractall(destination)

        parser.set_defaults(func=download)

    download = downloadParser(subparsers)


def pipelineParser(subparsers):
    parser = subparsers.add_parser("pipeline")
    subparsers = parser.add_subparsers()

    def triggerParser(subparsers):
        parser = subparsers.add_parser("trigger")
        parser.add_argument("pipelineRef")
        parser.add_argument("variables", nargs="*")

        def trigger(pipelineRef, variables, gitlab, token):
            assert token is None
            project, ref = pipelineRef.rsplit("/", 1)

            postAPI(
                gitlab,
                environ["CI_JOB_TOKEN"],
                f"/api/v4/projects/{ quoteAll(project) }/trigger/pipeline",
                **dict(
                    ref=ref,
                    **{
                        f"variables[{ k }]": v
                        for (k, v) in [var.split("=", 1) for var in variables]
                    },
                ),
            )

        parser.set_defaults(func=trigger)

    trigger = triggerParser(subparsers)


parser = ArgumentParser()
parser.add_argument("--gitlab", dest="gitlab", default="https://gitlab.com")
parser.add_argument("--token", dest="token", default=None)
subparsers = parser.add_subparsers()
artifacts = artifactsParser(subparsers)
pipeline = pipelineParser(subparsers)

args = vars(parser.parse_args())
func = args.pop("func")
func(**args)
