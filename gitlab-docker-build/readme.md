# gitlab-docker-build

## Easier building of Docker images with GitLab CI

When building a Docker image with GitLab CI — especially if using Docker-in-Docker GitLab runner, one often encounters build inefficiencies due to lack of Docker build caching. The official [GitLab CI instructions for Docker integration](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html) offer remedy, and this tool simply streamlines that remedy into a single script line.

Specifically, you can run 

```
gitlab-docker-build --cache-from DEFAULT NAME CONTEXT
```

And the following will occur:

1. `docker build` will be run to build `CONTEXT`
2. This Docker build will use cached layers from
 * `NAME-ci:ref-REF` image in your GitLab project's Docker registry, where `REF` is the git ref for which the pipeline was started (typically branch or tag name)
 * `NAME-ci:ref-DEFAULT` image in your GitLab CI registry, where `DEFAULT` is the name of your default branch
 * `NAME-ci:latest` image in your GitLab CI registry
3. The built docker image will be pushed to your GitLab project's Docker registry under the following tags:
 * `NAME-ci:iid-NNN`, where `NNN` is the per-project ID of the current pipeline
 * `NAME-ci:ref-REF`, where `REF` is the git ref for which the pipeline was started (typically branch or tag name)
 * `NAME-ci:latest`
4. If you are building a tag, the image will also be pushed to
 * `NAME:REF`, where `REF` is the tag for which the pipeline was started
 * `NAME:latest`
 
In other words, layers will be reused from:

 * The most recent successful pipeline on any branch or tag
 * The most recent successful pipeline on  the default branch
 * The most recent successful pipeline the same branch/tag as the current pipeline
 
and they will be made available for reuse on:

 * The next build on the same branch as the current pipeline, until another build takes places on the same branch
 * The next build on any other branch/tag, until another build takes place on any branch/tag

If you want layers from additional refs (besides the default branch) to be included in the build cache, you can use multiple `--cache-from` options to gitlab-docker-build

If, for any reason, you need to bypass the Docker layer cache, you can set the pipeline variable `CI_DOCKER_NOCACHE` to any non-empty string, which will cause the image to be rebuild from scratch. One circumstance when you may want to do that is when the cached layers have some stale information in them.

For example, if you are building an Ubuntu- or Debian-based image, you might run `apt-get update` in one step, and then later run `apt-get install` in a later step. Normally, `apt-get update` caches package information, which is then used by `apt-get install`, and this is desirable as it makes the install step faster. However, when you make changes to the install step (for example, to include additional apt packages in your Docker image), your Docker build will reuse the cached `apt-get update` layer from a previous build, which may have become stale. When this happens, your `apt-get install` step will mysteriously fail; you can remedy this by running a pipeline from GitLab UI with `CI_DOCKER_NOCACHE` set. After the no-cache pipeline succeeds, its built image will have the fresh apt cache.

Note that a better solution to this specific problem is to run `apt-get update && apt-get install` as a single step in your Dockerfile, so that any changes to the list of installed packages will also result in a refetching of the apt package list, without invalidating all the previous layers in the image. But if you need a large hammer to skip the entire layer cache, `CI_DOCKER_NOCACHE` is there for you.

And of course, gitlab-docker-build is available as a Docker image, so you can run it like so:

```
docker run registry.gitlab.com/cirecipes/gitlab-docker-build/gitlab-docker-build --cache-from DEFAULT NAME CONTEXT
```
