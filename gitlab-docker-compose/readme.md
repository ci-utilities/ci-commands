# gitlab-docker-compose

## Easier building of Docker images with `docker-compose` under GitLab CI

When building a Docker image with GitLab CI — especially if using Docker-in-Docker GitLab runner, one often encounters build inefficiencies due to lack of Docker build caching. The official [GitLab CI instructions for Docker integration](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html) offer remedy, and this tool simply streamlines that remedy into a single script line.

Specifically, you can run 

```
gitlab-docker-compose [--file COMPOSE …] [--cache-from FROM-REF … ] SERVICE …
```

And the following will occur:

1. The image IMAGE:TAG for each service will be identified
2. The following additional cache image tags will be identified for each service
  * IMAGE-ci:iid-NNN[-TAG] (NNN = pipeline ID)
  * IMAGE-ci:ref-REF[-TAG] (REF = pipeline branch/tag name)
  * IMAGE-ci:ref-FROM-REF[-TAG] (FROM-REF = additional branch/tag name as given)
  * IMAGE-ci:latest[-TAG]
3. The following output image tags will be identified for each service:
  * IMAGE-ci:iid-NNN[-TAG] (NNN = pipeline ID)
  * IMAGE-ci:ref-REF[-TAG] (REF = pipeline branch/tag name)
  * IMAGE-ci:latest[-TAG]
  * IMAGE:REF[-TAG] (REF = pipeline branch/tag name, only if building a tag)
  * IMAGE:latest[-TAG] (only if building a tag)
5. All cache images will be pulled (if they exist)
6. All output images will be built using the specified compose files. During the build, `cache_from` for each service will be overridden to include all cache images
7. All output images will be pushed to the Docker registry

In other words, layers will be reused from:

 * A previous successful Docker build in the same pipeline
 * The most recent successful pipeline on any branch or tag
 * The most recent successful pipeline on the default branch
 * The most recent successful pipeline the same branch/tag as the current pipeline
 
and they will be made available for reuse on:

 * The next build on the same branch as the current pipeline, until another build takes places on the same branch
 * The next build on any other branch/tag, until another build takes place on any branch/tag

If you want layers from additional refs (besides the current branch) to be included in the build cache, you can use `--cache-from` options to gitlab-docker-compose

Presumably, you will want to run your images after they are built, with either `docker-compose up` or `docker-compose run`. To do this, use `IMAGE-ci:iid-NNN-tag` in your compose file, thus making sure you're running the image from the same pipeline. For example, `${CI_REGISTRY}/${CI_PROJECT_PATH}/build-ci:iid-${CI_PIPELINE_IID}-tag`. 