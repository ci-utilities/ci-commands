#!/usr/bin/env python3

from argparse import ArgumentParser, RawDescriptionHelpFormatter
from os import environ
from subprocess import check_call, check_output, CalledProcessError, DEVNULL, Popen
import itertools
import re
import yaml
import tempfile

parser = ArgumentParser(
    formatter_class=RawDescriptionHelpFormatter, 
    description="""Build Docker images with docker-compose in GitLab CI environment, while maximizing use of Docker build cache.

Each service in the compose files is assumed to have an image of the form NAME:TAG. Before building the images with docker-compose, the following images (if available) are pulled for each service:

 * NAME-ci:iid-NNN[-TAG], where NNN is the CI pipeline IID (from $CI_PIPELINE_IID)
 * NAME-ci:ref-REF[-TAG], where REF is the CI pipeline ref (from $CI_COMMIT_REF_NAME)
 * NAME-ci:ref-REF[-TAG], where REF is passed in with --cache-from REF
 * NAME-ci:latest[-TAG]
 * NAME:REF[-TAG], where REF is the CI pipeline ref (from $CI_COMMIT_REF_NAME)
 * NAME:REF[-TAG], where REF is passed in with --cache-from REF
 * NAME:latest[-TAG]

During build, cache_from for each service is overridden to include those images. 
 
After building, the images for each service are pushed to the following Docker tags:

 * NAME-ci:iid-IID[-TAG], where IID is the CI pipeline IID (from $CI_PIPELINE_IID)
 * NAME-ci:ref-REF[-TAG], where REF is the CI pipeline ref (from $CI_COMMIT_REF_NAME)
 * NAME-ci:latest[-TAG]
 * NAME:REF[-TAG], where REF is the CI pipeline ref (from $CI_COMMIT_REF_NAME, only if the pipeline is running for a git tag)
 * NAME:latest[-TAG] (only if the pipeline is running for a git tag)""")

parser.add_argument("--dry-run", dest="dryRun", action="store_true", help="Print actions but don't run them")
parser.add_argument("--cache-from", dest="cacheFrom", default=[], action="append", metavar="REF", help="Include images tagged NAME-ci:ref-REF and NAME:REF in Docker build cache")
parser.add_argument("--file", dest="composeFile", action="append", metavar="COMPOSE", help="docker-compose config file name")
parser.add_argument("service", nargs='*', metavar="SERVICE", help="docker-compose service name")

args = parser.parse_args()

# Make a viable Docker tag out of a git ref name
def tagify(ref):
    tag = re.sub("[^a-zA-Z0-9_.-]", "-", ref)
    tag = re.sub("-+", "-", tag)
    tag = tag.lstrip(".").strip("-")[:128]
    return tag

# Make a temp file with given content
def tempContent(content, mode):
    f = tempfile.NamedTemporaryFile(mode)
    f.write(content)
    f.flush()
    return f

# Begin by running compose to dump the interpolated config
compose = ["docker-compose"] + sum([["--file", file] for file in args.composeFile], [])

# Parse the config and figure out image for each service
config = yaml.safe_load(check_output(compose + ["config"]))
services = config['services']

imageRE = '^(.*?)-ci(?::iid-\d+(-.*)?)?$'

images = {
    service: re.sub(imageRE, '\\1', services[service]['image'])
        for service in args.service
}

tags = {
    service: re.sub(imageRE, '\\2', services[service]['image'])
        for service in args.service
}

version = yaml.safe_load(open(args.composeFile[0]))['version']
cacheFrom = []
buildVariants = []

for service in args.service:
    commitRef = tagify(environ['CI_COMMIT_REF_NAME'])
    # CI tags for images (on all builds)
    ciImageBase = f"{ images[service] }-ci"
    ciImageIID = f"{ ciImageBase }:iid-{ environ['CI_PIPELINE_IID'] }{ tags[service] }"
    ciImageRef = f"{ ciImageBase }:ref-{ commitRef }{ tags[service] }"
    ciImageLatest = f"{ ciImageBase }:latest{ tags[service] }"
    ciImagesOther = [
        f"{ ciImageBase }:ref-{ tagify(ref) }{ tags[service] }" for ref in args.cacheFrom if ref != commitRef
    ]

    # Release tags for images (on tagged builds)
    imageBase = images[service]
    imageTag = f"{ imageBase }:{ commitRef }{ tags[service] }"
    imageLatest = f"{ imageBase }:latest{ tags[service] }"
    imagesTagOther = [
        f"{ imageBase }:{ tagify(ref) }{ tags[service] }" for ref in args.cacheFrom if ref != commitRef
    ]

    cacheFrom.extend(ciImagesOther + [ciImageIID, ciImageRef, ciImageLatest, imageTag, imageLatest])
    pushTo = [ciImageIID, ciImageRef, ciImageLatest] + ([imageTag, imageLatest] if environ.get("CI_COMMIT_TAG", None) else [])
    if not buildVariants:
        buildVariants = [{
            'version': version,
            'services': {}
        } for _ in pushTo]

    buildVariants = [{
        'version': variant['version'],
        'services': dict(variant['services'], **{service: {'image': tag}}),
    } for (tag, variant) in zip(pushTo, buildVariants)]

cacheFromByService = {
    'version': version,
    'services': {
        service: {
           'build': {
                'cache_from': cacheFrom
            }
        } for service in args.service
    },
}

if args.dryRun:
    check_call_orig = check_call
    def check_call(command, *args, **kwargs):
        print(f"Running command: {' '.join(command)}")

# Log into registry
check_call(["docker", "login", "-u", environ["CI_REGISTRY_USER"], "-p", environ["CI_REGISTRY_PASSWORD"], environ["CI_REGISTRY"]], stdout=DEVNULL)

# Pull the images used for cache; ok if any of them fail
print("Pulling build cache:")
for image in cacheFrom:
    try:
        check_call(["docker", "pull", "--quiet", image], stdout=DEVNULL)
        print(f"\t(hit ) { image }")
    except CalledProcessError as e:
        print(f"\t(miss) { image }")

# Build and push all variants
# Note that during builds each service is using *every* other service as its cache. There is basically no penalty for this since the images are getting pulled either way, and there is benefit in situations where one service uses a multistage build with layers A and B, only pushing B, and another service uses a multistage build with layers A and C and pushes both.
for idx, variant in enumerate(buildVariants):
    with tempContent(yaml.dump(cacheFromByService), "w") as cacheFromFile, \
        tempContent(yaml.dump(variant), "w") as variantFile:
        variantCompose = compose + [
            "--file", cacheFromFile.name, 
            "--file", variantFile.name, 
        ]
        if args.dryRun:
            print("Docker-compose configuration:")
            check_call_orig(variantCompose + ["config"])
        
        check_call(variantCompose + ["build"] + args.service, stdout=(None if idx == 0 else DEVNULL))
        check_call(variantCompose + ["push"] + args.service)
